require 'optparse'

module Gitlab
  module QA
    # rubocop:disable Metrics/AbcSize
    class Runner
      def self.run(args)
        Runtime::Scenario.define(:teardown, true)
        Runtime::Scenario.define(:run_tests, true)

        @options = OptionParser.new do |opts|
          opts.banner = 'Usage: gitlab-qa [options] Scenario URL [[--] path] [rspec_options]'

          opts.on('--no-teardown', 'Skip teardown of containers after the scenario completes.') do
            Runtime::Scenario.define(:teardown, false)
          end

          opts.on('--no-tests', 'Orchestrates the docker containers but does not run the tests. Implies --no-teardown') do
            Runtime::Scenario.define(:run_tests, false)
            Runtime::Scenario.define(:teardown, false)
          end

          opts.on_tail('-v', '--version', 'Show the version') do
            require 'gitlab/qa/version'
            puts "#{$PROGRAM_NAME} : #{VERSION}"
            exit
          end

          opts.on_tail('-h', '--help', 'Show the usage') do
            puts opts
            exit
          end

          begin
            opts.parse(args)
          rescue OptionParser::InvalidOption
            # Ignore invalid options and options that are passed through to the tests
          end
        end

        args.reject! { |arg| gitlab_qa_options.include?(arg) }

        if args.size >= 1
          Scenario
            .const_get(args.shift)
            .perform(*args)
        else
          puts @options
          exit 1
        end
      end
      # rubocop:enable Metrics/AbcSize

      def self.gitlab_qa_options
        @gitlab_qa_options ||= @options.top.list.map(&:long).flatten
      end
    end
  end
end
